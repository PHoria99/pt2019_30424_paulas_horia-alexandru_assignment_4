package businessLayer;

public class BaseProduct extends MenuItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9026050477179799608L;

	public BaseProduct(){
		this.setName("Rosii");
		this.setPrice(5);
	}
	
	public BaseProduct(String name, int price) {
		this.setName(name);
		this.setPrice(price);
	}
}
