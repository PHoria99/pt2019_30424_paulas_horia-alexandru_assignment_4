package businessLayer;
import java.io.IOException;
import java.util.ArrayList;


import javax.swing.table.DefaultTableModel;

public interface IRestaurantProcessing {
	public void createItem(String name, int price);
	public void createItem(String name, ArrayList<MenuItem> components);
	public void deleteItem(int id);
	public void editItem(int id, MenuItem replace);
	
	public void createOrder(int Table, ArrayList<MenuItem> order);
	public void createBill(int order) throws IOException;
	
	public default DefaultTableModel createItemTable(ArrayList<MenuItem> menu) {
		Object[][]data = new Object[menu.size()][3];
		String[] name = {"Name","Items","Price"};
		int i=0;
		for(MenuItem m: menu) {
			if(m.getClass().equals(BaseProduct.class)) {
				data[i][0] = ((BaseProduct) m).getName();
				data[i][1] = "";
				data[i][2] = ((BaseProduct) m).getPrice();
			}else {
				data[i][0] = ((CompositeProduct) m).getName();
				data[i][1] = ((CompositeProduct) m).getComponents();
				data[i][2] = ((CompositeProduct) m).getPrice();
			}
			i++;
		}
		return new DefaultTableModel(data,name);
	}
}
