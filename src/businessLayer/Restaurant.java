package businessLayer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import dataLayer.RestaurantSerializator;

@SuppressWarnings("deprecation")
public class Restaurant extends Observable implements IRestaurantProcessing{
	public ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
	public HashMap<Order, ArrayList<MenuItem>> customerOrders = new HashMap<Order, ArrayList<MenuItem>>();
	public ArrayList<Order> orders = new ArrayList<Order>();
	private Observer observer;
	public static int tables = 7;
	
	public Restaurant(Observer observer) throws NumberFormatException, FileNotFoundException, ClassNotFoundException, IOException 
	{
		menu = RestaurantSerializator.loadMenu();
		orders = RestaurantSerializator.loadOrders();
		customerOrders = RestaurantSerializator.loadCustomerOrders();
		this.observer = observer;
	}
	
	public ArrayList<Order> getTableOrders(int Table){
		ArrayList<Order> od = new ArrayList<Order>();
		for(Order o: orders) {
			if(o.Table == Table) {
				od.add(o);
			}
		}
		return od;
	}
	
	
	
	@Override
	public void createItem(String name, int price) {
		menu.add(new BaseProduct(name, price));
	}

	@Override
	public void createItem(String name, ArrayList<MenuItem> components) {
		CompositeProduct p = new CompositeProduct(name);
		for(MenuItem m: components) {
			p.add(m);
		}
		menu.add(p);
	}

	@Override
	public void deleteItem(int index) {
		int size = menu.size();
		for(int i = 0; i < size; i++){
			MenuItem x = menu.get(i);
			if(i == index) {
				menu.remove(x);
				break;
			}
		}
	}

	@Override
	public void editItem(int index, MenuItem replace) {
				menu.set(index, replace);
	}

	@Override
	public void createOrder(int Table, ArrayList<MenuItem> order) {
		Order o = new Order(orders.size(), Table, new Date());
		orders.add(o);
		customerOrders.put(o, order);
		String s = o.toString() + "|" + customerOrders.get(o).toString();
		setChanged();
		notifyObserver(this,s);
	}

	@Override
	public void createBill(int order) throws IOException {
		Order toBill = findOrder(order);
		ArrayList<MenuItem> itemsOrdered = customerOrders.get(toBill);
		BufferedWriter writer = null;
        try {
            //create a temporary file
            String fileName = "bill_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        	String timeLog = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(Calendar.getInstance().getTime());
            File logFile = new File(fileName);

            // This will output the full path where the file will be written to...
            System.out.println(logFile.getCanonicalPath());
            writer = new BufferedWriter(new FileWriter(logFile));
            
            String billItself = "Date: " + timeLog + "\n";
            for(MenuItem m : itemsOrdered)
            {
            	billItself = m.getName() + "      " + m.getPrice() + "\n";
            }
            billItself += "-------------------------------------------------------\nTOTAL:       ";
            billItself += getOrderPrice(itemsOrdered);
            writer.write(billItself);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
                // Close the writer regardless of what happens...
                writer.close();
        }
		
	}
	
	private static int getOrderPrice(ArrayList<MenuItem> order) {
		int fullPrice =0;
		for(MenuItem m : order) {
			fullPrice += m.getPrice();
		}
		return fullPrice;
	}
	
	public void notifyObserver(Observable observable, String s)
	{
		String arr[] = s.split("|");
		observer.update(observable, arr);
	}
	
	private Order findOrder(int id) {
		for(Order o: this.orders) {
			if(o.OrderID == id) return o;
		}
		return null;
	}
}
