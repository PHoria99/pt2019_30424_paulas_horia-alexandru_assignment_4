package presentationLayer;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class AdminGUI {

	private JFrame frmAdmin;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblName;
	private JLabel lblPrice;
	private JButton btnBuyBasicItem;
	private JComboBox comboBox;
	private JLabel lblItem;
	private JButton button;
	private JButton btnAddItemTo;
	private JButton btnUpdateItem;
	private Restaurant restaurant;
	private CompositeProduct currentComposite = new CompositeProduct();
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnModifyPrice;
	private JButton btnDeleteComponent;

	/**
	 * Create the application.
	 */
	public AdminGUI(Restaurant rest) {
		restaurant = rest;
		initialize();
		this.frmAdmin.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAdmin = new JFrame();
		frmAdmin.setTitle("Admin");
		frmAdmin.setBounds(100, 100, 450, 300);
		this.frmAdmin.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frmAdmin.getContentPane().add(panel, BorderLayout.CENTER);
		
		lblName = DefaultComponentFactory.getInstance().createLabel("Name");
		panel.add(lblName);
		
		textField_1 = new JTextField();
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		lblPrice = DefaultComponentFactory.getInstance().createLabel("Price");
		panel.add(lblPrice);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		
		btnBuyBasicItem = new JButton("Buy Basic Item");
		btnBuyBasicItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restaurant.menu.add(new BaseProduct(textField_1.getText(), Integer.parseInt(textField.getText())));
				fillBox();
				fillTable();
			}
		});
		panel.add(btnBuyBasicItem);
		
		lblItem = DefaultComponentFactory.getInstance().createLabel("Item");
		panel.add(lblItem);
		
		comboBox = new JComboBox<String>();
		fillBox();
		panel.add(comboBox);
		
		button = new JButton("Add to Composite");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentComposite.setName(textField_1.getText());
				MenuItem p = findMenuItem(comboBox.getSelectedItem().toString());
				currentComposite.add(p);
			}
		});
		panel.add(button);
		
		btnAddItemTo = new JButton("Add Item to Menu");
		btnAddItemTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restaurant.menu.add(currentComposite);
				currentComposite = new CompositeProduct();
				fillBox();
				fillTable();
			}
		});
		panel.add(btnAddItemTo);
		
		btnUpdateItem = new JButton("Delete Item");
		btnUpdateItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restaurant.menu.remove(findMenuItem(comboBox.getSelectedItem().toString()));
				fillBox();
				fillTable();
			}
		});
		panel.add(btnUpdateItem);
		
		scrollPane = new JScrollPane();
		table = new JTable();
		fillTable();
		
		btnModifyPrice = new JButton("Modify Price");
		btnModifyPrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				findMenuItem(comboBox.getSelectedItem().toString()).setPrice(Integer.parseInt(textField.getText()));
				fillTable();
			}
		});
		panel.add(btnModifyPrice);
		
		btnDeleteComponent = new JButton("Delete Component");
		btnDeleteComponent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deleteComponent();
			}
		});
		panel.add(btnDeleteComponent);
		scrollPane.setViewportView(table);
		panel.add(scrollPane);
		
	}

	@SuppressWarnings("unchecked")
	private void fillBox()
	{
		this.comboBox.removeAllItems();
		for(MenuItem m : restaurant.menu) {
			this.comboBox.addItem(m.getName());
		}
	}
	
	private void fillTable()
	{
		this.table.removeAll();
		Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		Vector<String> name = new Vector<>();
		name.add("Name"); name.add("Components"); name.add("Price"); 
		
		for(MenuItem m: restaurant.menu) {
			Vector<Object> row = new Vector<>();
			row.add(m.getName());
			if(m instanceof CompositeProduct) {
				row.addElement(((CompositeProduct) m).getComponents());
			}
			else row.addElement("");
			row.add(m.getPrice());
			rows.add(row);
		}
		
		table.setModel(new DefaultTableModel(rows,name));
	}
	
	private MenuItem findMenuItem(String name) {
		for(MenuItem m : restaurant.menu) {
			if(m.getName().equals(name)) return m;
		}
		return null;
	}
	
	private void deleteComponent() {
		CompositeProduct composite = (CompositeProduct) findMenuItem((String) table.getValueAt(table.getSelectedRow(), 0));
		String toBeDeletedName = (String) comboBox.getSelectedItem().toString();
		for(MenuItem m : composite.items) {
			if(m.getName().equals(toBeDeletedName))
				composite.items.remove(m);
		}
		fillTable();
	}
}
