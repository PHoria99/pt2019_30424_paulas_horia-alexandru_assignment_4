package presentationLayer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

@SuppressWarnings("deprecation")
public class ChefGU implements Observer, IRestaurantProcessing{
	private JFrame frmChef;
	@SuppressWarnings("unused")
	private Restaurant restaurant;
	private JTable orders;
	
	public ChefGU(Restaurant r) {
		this.initialize();
		this.restaurant = r;
		this.frmChef.setVisible(true);
	}
	
	private void initialize()
	{
		this.frmChef = new JFrame();
		this.frmChef.setSize(400, 400);
		frmChef.setTitle("Chef");
		this.frmChef.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		JScrollPane scrollPane = new JScrollPane();
		orders=new JTable();
		Object[][] entries = new Object[0][2];
		String[] columnNames = {"OrderID", "Items"};
		orders.setModel(new DefaultTableModel(entries, columnNames));
		scrollPane.setViewportView(orders);
		frmChef.getContentPane().add(scrollPane);
	}

	
	@Override
	public void update(Observable o, Object arg) {
		String[] s = (((String) arg).split(":|!"));
		String[] r1 = s[1].split("=|,");
		
		((DefaultTableModel) orders.getModel()).addRow(new Object[]{r1[1],s[3]});
	}

	//No need to implement these methods as we don't use them, but we have this to respect the given diagram
	@Override
	public void createItem(String name, int price) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createItem(String name, ArrayList<MenuItem> components) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deleteItem(int id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void editItem(int id, MenuItem replace) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createOrder(int Table, ArrayList<MenuItem> order) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createBill(int order) throws IOException {
		// TODO Auto-generated method stub
	}
}
