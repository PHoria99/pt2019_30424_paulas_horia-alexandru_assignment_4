package presentationLayer;
import java.io.FileNotFoundException;
import java.io.IOException;

import businessLayer.Restaurant;


public class Main {
	private static Restaurant rest;
	private static ChefGU chefGUI;
	@SuppressWarnings("unused")
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException, ClassNotFoundException, IOException {
			chefGUI = new ChefGU(rest);
			rest = new Restaurant(chefGUI);
			GUI gui = new GUI(rest);
			AdminGUI adminGUI = new AdminGUI(rest);
			WaiterGUI waiterGUI = new WaiterGUI(rest);
			
	}

}
